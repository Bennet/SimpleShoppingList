<?php
define( 'VERSION', '0.1' );

/*--------------------------------------------------------------
# MSQL Settings
--------------------------------------------------------------*/
/** MySQL database name */
define( 'DB_NAME', 'YOUR_DB_NAME' );

/** MySQL database username */
define( 'DB_USER', 'YOUR_DB_USER' );

/** MySQL database password */
define( 'DB_PASSWORD', 'YOUR_DB_PASSWORD' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );