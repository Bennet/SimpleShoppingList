<?php session_start( ['cookie_lifetime' => 31557600] ); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="robots" content="noindex, nofollow">
    <link rel="manifest" href="app.webmanifest">
    <link rel="stylesheet" href="assets/pico.min.css">
    <link rel="stylesheet" href="assets/style.css">
    <script src="assets/jquery.min.js"></script>
    <script src="assets/jquery-ui.min.js"></script>
    <script src="assets/jquery-ui.touch-punch.min.js"></script>
    <script src="assets/main.js"></script>
    <script> PWAServiceWorker(); </script>
    <title>Simple Shopping List 🛒</title>
</head>
<body>
    <?php if ( ! $_SESSION['user'] ) : ?>
        <h1 class="page-title">Simple Shopping List</h1>

        <form type="POST">
            <input id="user" type="text" name="user" placeholder="User">
            <input id="password" type="password" name="password" placeholder="Password">
            <input type="submit" value="Login">
        </form>

        <div id="message"></div>

        <script>
            $("form").submit( function(event) {
                event.preventDefault();

                $.ajax( {
                    type: "POST",
                    url: "ajax-handler.php",
                    data: {
                        action: 'login',
                        user: $(this).find('#user').val(),
                        password: SHA256($(this).find('#password').val()), 
                    },
                    success: function(data) {
                        var message = (data) ? 'Login successfull' : 'Something went wrong.';
                        $("#message").html(message);

                        if (data) {
                            location.reload(true);
                        }
                    }  
                });

                $("form")[0].reset();
            })

        </script>
    <?php else: ?>
    
    <h1 class="page-title">hi <?php echo $_SESSION['user']; ?> 👋</h1>

    <form id="add_to_list" type="GET" name="add_to_list" autocomplete="off">
        <input type="text" name="item" placeholder="Add to list...">
        <input type="submit" value="+">
    </form>

    <small class="secondary">Your list state: <span class="list-date"></span></small>
    <div class="list-state"></div>
    <div id="lists"></div>
    <a id="logout" href="logout">Logout</a>

    <script>
        // Handle add
        $("#add_to_list").submit( function(event) {
            event.preventDefault();

            $.ajax( {
                type: "POST",
                url: "ajax-handler.php",
                data: {
                    action: "add",
                    item: $(this).find('input[type="text"]').val(),
                },
                success: function(data) {
                    loadLists();
                    listState('saved');
                }  
            });

            $("#add_to_list")[0].reset();
        });

        // Display lists 
        $(document).ready(loadLists);

        // Handle liste state - if existing item value (name) changed
        $(document).on('input', '.item input', '', listState);

        // Update existing item on change 
        $(document).on('change', '.item input', this, addExistingItemToList); 

        // Handle suggestions
        $(document).on('input', '#add_to_list input', this, suggestions);
        $(document).on('click', '#suggestions li', this, addExistingItemToList);

        // Handle delete
        $(document).on( "click", ".delete", function(event) {
            event.preventDefault();

            $.ajax( {
                type: "POST",
                url: "ajax-handler.php",
                data: { 
                    action: "delete",
                    id: $(this).attr('data-item')
                },
                success: function() {
                    loadLists();
                    listState('saved');
                }  
            });

        });

        // Handle logout
        $("#logout").on( 'click', function(event) {
            event.preventDefault();

            $.ajax( {
                type: "POST",
                url: "ajax-handler.php",
                data: { 
                    action: "logout",
                },
                success: function() {
                    location.reload(true);
                }  
            });

        });
    </script>

    <?php endif; ?>
    
</body>
</html>