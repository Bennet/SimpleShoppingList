/**
 * Set JS Globals
 */
if ( ! window.simpleShoppingList ) {
	window.simpleShoppingList = {
		items: {
			unchecked: null,
			checked: null,
		},
		listState: {
			state: null,
			blocked: false,
			timeout: null,
		}
	}
}

/**
 * Init Suggestions
 *
 * @return void
 */
function suggestions() {
	if ( this.value.length > 0 ) {
		
		var suggestions = getSuggestions(this.value);
		var markup      = prepareSuggestions(suggestions);
		(markup) ?  showSuggestions(markup) : removeSuggestions();

	} else {
		removeSuggestions();
	}
}

/**
 * Get Suggestions from checked list
 * 
 * @param {String} input 
 * @return {Array} suggestions
 */
function getSuggestions(input) {
	const items         = window.simpleShoppingList.items.checked;
	const suggestions   = [];
	input = input.toLowerCase();
  
	for(let key in items){
		const name = items[key].name.toLowerCase();
		if ( name.startsWith(input)  ) {
			suggestions[key] = items[key];
		}
	}

	return suggestions;
}

/**
 * Prepare suggestions, build markup
 * 
 * @param {Array} suggestions 
 * @return {String} markup
 */
function prepareSuggestions(suggestions) {
	var markup = '<div id="suggestions"><ul>';

	suggestions.forEach(element => {
		markup += '<li data-item="' + element.id + '">' + element.name + '</li>';
	});

	markup += '</ul></div>';

	return markup;
}

/**
 * Display suggestions in frontend. 
 * Insert or replace if container already exist
 * 
 * @param {String} markup 
 */
function showSuggestions(markup) {
	var container = $('#suggestions');

	if ( ! container.length ) {
		$(markup).insertAfter('#add_to_list input[type="text"]');
	} else {
		$(container).replaceWith(markup);
	}
}

/**
 * Clear value and focus input of #add_to_list
 *
 * @return void
 */
function clearAndFocus() {
	$('#add_to_list input[type="text"]').val('');
	$('#add_to_list input[type="text"]').focus();
}

/**
 * Remove suggestions container
 *
 * @return void
 */
function removeSuggestions() {
	$('#suggestions').remove();
}

/**
 * Add an already existing (checked) item again to (unchecked) list.
 * If .is('li') -> function is fired by a suggestion selection
 */
function addExistingItemToList() {
	var suggestion = $(this).is('li');
	var data = { 
			action: "update",
			id: $(this).attr('data-item'),
			item: ( $(this).val() != "on" ) ? $(this).val() : null,
			state: ( $(this).is(":checked") ) ? false : true,
			order: ( $(this).is(":checkbox") ) ? '' : false,
	};

	if ( suggestion ) {
		data.state = true;
		data.order = false;
	}

	$.ajax( {
		type: "POST",
		url: "ajax-handler.php",
		data,
		success: function() {
			if ( suggestion ) {
				removeSuggestions();
				clearAndFocus();
			}
			dbOrderUpdate();
			loadLists();
			listState('saved');
		}  
	});
}

/**
 * Fire function to shrink the order max number in db.
 *
 * @return void
 */
function dbOrderUpdate() {
	$.ajax( {
		type: "POST",
		url: "ajax-handler.php",
		data: { 
			action: "db_order_update",
		}
	});
}

/**
 * Load unchecked and checked list
 *
 * @return void
 */
function loadLists() {
	$.ajax( {
		type: "POST",
		url: "ajax-handler.php",
		data: {
			action: "load_lists"
		},
		success: function(data) {
			data = JSON.parse(data);

			// update global js data
			 window.simpleShoppingList.items = {
				unchecked: data.items.unchecked,
				checked: data.items.checked
			}
			
			// update html & init sorting possibility
			$('#lists').html(data.lists_html);
			sortList();

			// @todo if suggestion are open and suggested item gets deleted -> list should refresh witout deleted item
			//suggestions();
		}  
	});

	setListTimestamp();
}

/**
 * Make unchecked list sortable
 */
function sortList() {
    $( ".sortable_list" ).sortable({
        containment: "#show_list",
        items: "> li:not(#new-item)",
		handle: ".drag",
        axis: "y",
        cursor: "move",
        update: function(event, ui) {
            var ids = $( ".sortable_list" ).sortable( "toArray" );

            $.ajax( {
                type: "POST",
                url: "ajax-handler.php",
                data: { 
                    action: "update_list_order",
                    ids: ids
                },
                success: function() {
                    listState('saved');
                }  
            });
        },
    });
}

/**
 * List timestamp, when the list was updated
 */
function setListTimestamp() {
    let current = new Date();
    let cMinutes = ( current.getMinutes() < 10 ) ? '0' + current.getMinutes() : current.getMinutes();
    let cDate = current.getDate() + '.' + (current.getMonth() + 1) + '.' + current.getFullYear();
    let cTime = current.getHours() + ":" + cMinutes;
    let dateTime = cDate + ', ' + cTime;

    $('.list-date').html(dateTime);
}

/**
 * Set list state for user - list is saved or or not.
 * Play some animation.
 * @param {String} state 
 */
function listState( state ) {
	const sSL = window.simpleShoppingList.listState;
    sSL.state = ( 'saved' === state ) ? 'saved' : 'not-saved';

    showBar();

    if ( 'not-saved' === sSL.state ) {
        notSaved();
        sSL.blocked = true;
    }

    if ( sSL.blocked ) {
        clearTimeout(sSL.timeout);
    }
    
    if ( 'saved' === sSL.state ) {
        saved();
        sSL.timeout = setTimeout(() => {
            setTimeout(() => {
                hideBar();
            }, 350);
        }, 2500);
    }

    // show bar
    function showBar(){
        if ( ! $('.list-state').hasClass('is-visible') ) {
            $('.list-state').addClass('is-visible');
        }
    }

    // hide bar 
    function hideBar() {
        $('.list-state').removeClass('is-visible');
    }

    // not saved
    function notSaved() {
        $('.list-state').removeClass('saved');
        if ( ! $('.list-state').hasClass('not-saved') ) {
            var msg = '<strong>Not saved</strong>';
            $('.list-state').html(msg);
            $('.list-state').addClass('not-saved');
        }
    }

    // saved
    function saved() {
        $('.list-state').removeClass('not-saved');
        if ( ! $('.list-state').hasClass('saved') ) {
            var msg = '<strong>Saved</strong>';
            $('.list-state').html(msg);
            $('.list-state').addClass('saved');
        }
    }
}

/**
 * Registrate serviceWorke for PWA support
 */
function PWAServiceWorker() {
	if ('serviceWorker' in navigator) {
		// Register a service worker hosted at the root of the
		// site using the default scope.
		navigator.serviceWorker.register('/assets/sw.js').then(function(registration) {
		  console.log('Service worker registration succeeded:', registration);
		}, /*catch*/ function(error) {
		  console.log('Service worker registration failed:', error);
		});
	} else {
		console.log('Service workers are not supported.');
	}
}

/**
*
*  Secure Hash Algorithm (SHA256)
*  http://www.webtoolkit.info/
*
*  Original code by Angel Marin, Paul Johnston.
*
**/
function SHA256(s){
 
	var chrsz   = 8;
	var hexcase = 0;
 
	function safe_add (x, y) {
		var lsw = (x & 0xFFFF) + (y & 0xFFFF);
		var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
		return (msw << 16) | (lsw & 0xFFFF);
	}
 
	function S (X, n) { return ( X >>> n ) | (X << (32 - n)); }
	function R (X, n) { return ( X >>> n ); }
	function Ch(x, y, z) { return ((x & y) ^ ((~x) & z)); }
	function Maj(x, y, z) { return ((x & y) ^ (x & z) ^ (y & z)); }
	function Sigma0256(x) { return (S(x, 2) ^ S(x, 13) ^ S(x, 22)); }
	function Sigma1256(x) { return (S(x, 6) ^ S(x, 11) ^ S(x, 25)); }
	function Gamma0256(x) { return (S(x, 7) ^ S(x, 18) ^ R(x, 3)); }
	function Gamma1256(x) { return (S(x, 17) ^ S(x, 19) ^ R(x, 10)); }
 
	function core_sha256 (m, l) {
		var K = new Array(0x428A2F98, 0x71374491, 0xB5C0FBCF, 0xE9B5DBA5, 0x3956C25B, 0x59F111F1, 0x923F82A4, 0xAB1C5ED5, 0xD807AA98, 0x12835B01, 0x243185BE, 0x550C7DC3, 0x72BE5D74, 0x80DEB1FE, 0x9BDC06A7, 0xC19BF174, 0xE49B69C1, 0xEFBE4786, 0xFC19DC6, 0x240CA1CC, 0x2DE92C6F, 0x4A7484AA, 0x5CB0A9DC, 0x76F988DA, 0x983E5152, 0xA831C66D, 0xB00327C8, 0xBF597FC7, 0xC6E00BF3, 0xD5A79147, 0x6CA6351, 0x14292967, 0x27B70A85, 0x2E1B2138, 0x4D2C6DFC, 0x53380D13, 0x650A7354, 0x766A0ABB, 0x81C2C92E, 0x92722C85, 0xA2BFE8A1, 0xA81A664B, 0xC24B8B70, 0xC76C51A3, 0xD192E819, 0xD6990624, 0xF40E3585, 0x106AA070, 0x19A4C116, 0x1E376C08, 0x2748774C, 0x34B0BCB5, 0x391C0CB3, 0x4ED8AA4A, 0x5B9CCA4F, 0x682E6FF3, 0x748F82EE, 0x78A5636F, 0x84C87814, 0x8CC70208, 0x90BEFFFA, 0xA4506CEB, 0xBEF9A3F7, 0xC67178F2);
		var HASH = new Array(0x6A09E667, 0xBB67AE85, 0x3C6EF372, 0xA54FF53A, 0x510E527F, 0x9B05688C, 0x1F83D9AB, 0x5BE0CD19);
		var W = new Array(64);
		var a, b, c, d, e, f, g, h, i, j;
		var T1, T2;
 
		m[l >> 5] |= 0x80 << (24 - l % 32);
		m[((l + 64 >> 9) << 4) + 15] = l;
 
		for ( var i = 0; i<m.length; i+=16 ) {
			a = HASH[0];
			b = HASH[1];
			c = HASH[2];
			d = HASH[3];
			e = HASH[4];
			f = HASH[5];
			g = HASH[6];
			h = HASH[7];
 
			for ( var j = 0; j<64; j++) {
				if (j < 16) W[j] = m[j + i];
				else W[j] = safe_add(safe_add(safe_add(Gamma1256(W[j - 2]), W[j - 7]), Gamma0256(W[j - 15])), W[j - 16]);
 
				T1 = safe_add(safe_add(safe_add(safe_add(h, Sigma1256(e)), Ch(e, f, g)), K[j]), W[j]);
				T2 = safe_add(Sigma0256(a), Maj(a, b, c));
 
				h = g;
				g = f;
				f = e;
				e = safe_add(d, T1);
				d = c;
				c = b;
				b = a;
				a = safe_add(T1, T2);
			}
 
			HASH[0] = safe_add(a, HASH[0]);
			HASH[1] = safe_add(b, HASH[1]);
			HASH[2] = safe_add(c, HASH[2]);
			HASH[3] = safe_add(d, HASH[3]);
			HASH[4] = safe_add(e, HASH[4]);
			HASH[5] = safe_add(f, HASH[5]);
			HASH[6] = safe_add(g, HASH[6]);
			HASH[7] = safe_add(h, HASH[7]);
		}
		return HASH;
	}
 
	function str2binb (str) {
		var bin = Array();
		var mask = (1 << chrsz) - 1;
		for(var i = 0; i < str.length * chrsz; i += chrsz) {
			bin[i>>5] |= (str.charCodeAt(i / chrsz) & mask) << (24 - i%32);
		}
		return bin;
	}
 
	function Utf8Encode(string) {
		string = string.replace(/\r\n/g,"\n");
		var utftext = "";
 
		for (var n = 0; n < string.length; n++) {
 
			var c = string.charCodeAt(n);
 
			if (c < 128) {
				utftext += String.fromCharCode(c);
			}
			else if((c > 127) && (c < 2048)) {
				utftext += String.fromCharCode((c >> 6) | 192);
				utftext += String.fromCharCode((c & 63) | 128);
			}
			else {
				utftext += String.fromCharCode((c >> 12) | 224);
				utftext += String.fromCharCode(((c >> 6) & 63) | 128);
				utftext += String.fromCharCode((c & 63) | 128);
			}
 
		}
 
		return utftext;
	}
 
	function binb2hex (binarray) {
		var hex_tab = hexcase ? "0123456789ABCDEF" : "0123456789abcdef";
		var str = "";
		for(var i = 0; i < binarray.length * 4; i++) {
			str += hex_tab.charAt((binarray[i>>2] >> ((3 - i%4)*8+4)) & 0xF) +
			hex_tab.charAt((binarray[i>>2] >> ((3 - i%4)*8  )) & 0xF);
		}
		return str;
	}
 
	s = Utf8Encode(s);
	return binb2hex(core_sha256(str2binb(s), s.length * chrsz));
 
}