<?php
include_once('config.php');
  try {
    $dbh = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME .'', DB_USER, DB_PASSWORD);

    $id     = ( ! empty( $_POST['id'] ) ) ? test_input( $_POST['id'] ) : null;
    $ids    = ( ! empty( $_POST['ids'] ) ) ? test_input( $_POST['ids'] ) : null;
    $name   = ( ! empty( $_POST['item'] ) ) ? test_input( $_POST['item'] ) : null;
    $state  = ( ! empty( $_POST['state'] ) ) ? test_input( $_POST['state'] ) : null;
    $order  = ( ! empty( $_POST['order'] ) && "99999" != $_POST['id'] ) ? test_input( $_POST['order'] ) : getLatestItemOrder( $dbh );
    // if is new item input - change action from "update" to "add"
    $action = ( "99999" === $id ) ? 'add' : test_input( $_POST['action'] );
    $query  = '';

    switch ($action) {
        case 'load_lists':
            load_lists( $dbh );
            break;

        case 'add':
            $query = addItem( $name, $order );
            break;

        case 'update':
            $query = updateItem( $id, $name, $state, $order );
            break;

        case 'update_list_order':
            $query = update_list_order( $ids );
            break;
        
        case 'db_order_update':
            db_order_update( $dbh );
            break;

        case 'delete':
            $query = delete_item( $id );
            break;

        case 'login':
            $user = ( ! empty( $_POST['user'] ) ) ? test_input( $_POST['user'] ) : null;
            $pass = ( ! empty( $_POST['password'] ) ) ? test_input( $_POST['password'] ) : null;
            login( $dbh, $user, $pass );
            break;

        case 'logout':
            logout();
            break;
        
        default:
            # code...
            break;
    }

    if ( $query ) {
        $sth = $dbh->prepare($query);
        $sth->execute();
    }

    // close connection
    $dbh = null;
 } catch (PDOException $e) {
    print "Error!: " . $e->getMessage() . "<br/>";
    die();
}

/**
 * Load unchecked and checked list
 *
 * @param [object] $dbh
 * @return void
 */
function load_lists( $dbh ) {
    $markup = "";
    $values = [];

    $queries = array(
        "unchecked_list" => "SELECT id, name FROM items WHERE items.active = true ORDER BY order_in_list ASC",
        "checked_list"   => "SELECT id, name, active FROM items WHERE items.active = false ORDER BY timestamp DESC"
    );

    foreach ( $queries as $key => $query ) {
        $sth = $dbh->prepare($query);
        $sth->execute();
        $result = $sth->fetchAll(PDO::FETCH_ASSOC);

        if ( 'unchecked_list' === $key ) {
            $values['items']['unchecked'] = $result;
            $markup .= '<form id="show_list" name="show_list" type="GET"><ul class="sortable_list">';
            $drag = '<div class="drag"><span></span><span></span><span></span><span></span><span></span><span></span></div>';
            foreach ( $result as $item ) {
                $markup .= '<li class="item" id="' . $item['id'] . '"><input type="checkbox" data-item="' . $item['id'] . '" name="' . $item['id'] . '"><input type="text" data-item="' . $item['id'] . '" value="' . $item['name'] . '">' . $drag . '</li>';
            }
            $markup .= '<li id="new-item" class="item"><input type="text" data-item="' . '99999' . '" placeholder="' . 'New Item' . '"></li>';
            $markup .= '</ul></form>';
        }

        if ( 'checked_list' === $key ) {
            $values['items']['checked'] = $result;
            $markup .= '<form id="checked_list" name="checked_list" type="GET"><ul>';
            foreach ( $result as $item ) {
                $markup .= '<li class="item secondary"><input type="checkbox" data-item="' . $item['id'] . '" name="' . $item['id'] . '" checked="checked"><s>' . $item['name'] . '</s><a class="delete secondary" data-item="' . $item['id'] . '" href="#delete">+</a></li>';
            }
            $markup .= '</ul></form>';
        }

    }

	$values['lists_html'] = $markup;

	echo json_encode($values);
}

/**
 * Add new item
 *
 * @param [string] $name
 * @return void
 */
function addItem( $name, $order ) {

    $query = "INSERT INTO items (name, active, order_in_list) VALUES ('" . $name . "', true, '" . $order . "')"; 

    return $query;
}

/**
 * Get highest order index from items
 *
 * @param [object] $dbh
 * @return [array] $order
 */
function getLatestItemOrder( $dbh ) {
    $query = "SELECT order_in_list FROM items ORDER BY order_in_list DESC LIMIT 1";

    $sth = $dbh->prepare($query);
    $sth->execute();
    $result = $sth->fetchAll();
    $order = $result[0]['order_in_list'] + 1;

    return $order;
}

/**
 * Update item
 *
 * @param [int] $id
 * @param [string] $name
 * @param [boolean] $state
 * @return void
 */
function updateItem( $id, $name = null, $state = null, $order = false ) {
    $query = '';

    if ( ! is_null( $name ) ) {
        $query .= "UPDATE items SET name = '" . $name . "' WHERE items.id = '" . $id . "';";
    }

    if ( ! is_null( $state ) ) {
        $query .= "UPDATE items SET active = " . $state . " WHERE items.id = '" . $id . "';";
    }

    // update order if isset -> not when change item name
    if ( "false" != $order ) {
        $query .= "UPDATE items SET order_in_list = " . $order . " WHERE items.id = '" . $id . "';";
    }
    
    return $query;
}

/**
 * Used to update items order in db if list was sorted via drag and drop
 *
 * @param [array] $ids
 * @return [string] $query
 */
function update_list_order( $ids ) {
    $query = '';

    foreach ( $ids as $index => $id ) {

            $index = $index + 1;
            $query .= "UPDATE items SET order_in_list = " . $index . " WHERE items.id = '" . $id . "';";
    }

    return $query;
}

/**
 * Reset order numbers in the database.
 * Order of items will be the same, just the numbers will be redefined -> starting from 1
 *
 * @param [object] $dbh
 * @return void
 */
function db_order_update( $dbh ) {

    // get all items in current order
    $query = "SELECT id, active FROM items ORDER BY order_in_list ASC";
    
    $sth = $dbh->prepare($query);
    $sth->execute();
    $result = $sth->fetchAll(PDO::FETCH_ASSOC);

    $query = "";
    $count = 1;
    // loop items and set new order number (order will be the same -> just the number is new)
    foreach ( $result as $item ) {
        
        if ( true == $item['active'] ) {
            $query .= "UPDATE items SET order_in_list = " . $count . " WHERE items.id = '" . $item['id'] . "';";
            $count ++;
        } else {
            // all checked items get order numer 0
            $query .= "UPDATE items SET order_in_list = 0 WHERE items.id = '" . $item['id'] . "';";
        }
        
    }

    $sth = $dbh->prepare($query);
    $sth->execute();

}

/**
 * Delete checked item
 *
 * @param [integer] $id
 * @return void
 */
function delete_item( $id ) {
    
    $query = "DELETE FROM items WHERE items.id = '" . $id . "';";

    return $query;
}

/**
 * User login
 *
 * @param [object] $dbh
 * @param [string] $user
 * @param [string] $pass
 * @return void
 */
function login( $dbh, $user, $pass) {
    $login  = false;
    $pass   = hash( 'sha256', $pass );
    $query  = "SELECT username FROM users WHERE users.username = '" . $user . "' AND users.password = '" . $pass . "'";

    $sth = $dbh->prepare($query);
    $sth->execute();
    $result = $sth->fetchAll(PDO::FETCH_ASSOC);

    if ( $result ) {
        $login = true;
        session_start( ['cookie_lifetime' => 31557600] );
        $_SESSION['user'] = $user;
    }

    echo $login;
}

/**
 * User logout
 *
 * @return void
 */
function logout() {
    session_start();
    unset( $_SESSION["user"] );
}

/**
 * Test and validate input data
 * 
 * @param   string  $data
 * @return  string
 */
function test_input( $data ) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);

    return $data;
  }