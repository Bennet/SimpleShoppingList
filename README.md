# Simple Shopping List - Work in Progress

This is a simple shared shopping list. This means that several people can add, edit or tick off items on the list. 
The shopping list can be used on the computer in the browser, on the mobile phone in the mobile version and also as a web app.
It is a personal replacement for the Google Notes app that was previously used privately.

## :warning: Current state :warning:
At the time of updating the readme: no active further development is currently taking place. Even if there is potential for improvement in many areas, the application can and is already being actively used.

In the future, the project will be further developed, new features will be made available, security will be improved and the installation process will be simplified.
Use at your own risk.

## Screenshots
| Desktop | Mobile login | Mobile list |
|---|---|---|
| ![](https://codeberg.org/Bennet/SimpleShoppingList/raw/branch/main/.screenshots/simple-shopping-list-desktop.png)  |  ![](https://codeberg.org/Bennet/SimpleShoppingList/raw/branch/main/.screenshots/simple-shopping-list-mobile-login.png)  |  ![](https://codeberg.org/Bennet/SimpleShoppingList/raw/branch/main/.screenshots/simple-shopping-list-mobile-list.png) |

(There is a light and a dark mode available.)

## Installation
Requirements:
- PHP server
- MySQL database

### Step 1: Set up database
**This process will be simplified in the future.**
1. Set up the database with a database name, user and password.
2. Login into your database
3. Import structure / demo data:
````SQL
CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `count` int(11) DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  `order_in_list` int(11) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

INSERT INTO `items` (`id`, `name`, `count`, `active`, `order_in_list`, `timestamp`) VALUES
(1, 'Apple', NULL, 1, 1, '2022-03-21 16:29:48'),
(2, 'Bread', NULL, 1, 2, '2022-03-15 14:19:44'),
(3, 'Water', NULL, 1, 3, '2022-03-16 12:03:26'),
(4, 'Milk', NULL, 1, 4, '2022-08-08 14:17:31'),
(5, 'Juice', NULL, 0, 0, '2022-03-16 10:59:00'),
(6, 'Rice', NULL, 0, 0, '2023-05-06 11:24:51'),
(7, 'Salad', NULL, 0, 0, '2022-03-16 12:03:26');

CREATE TABLE `users` (
  `ID` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;


INSERT INTO `users` (`ID`, `username`, `password`) VALUES
(1, 'bennet', '7b3d979ca8330a94fa7e9e1b466d8b99e0bcdea1ec90596c0dcc8d7ef6b4300c'),
(2, 'sophie', '6016bcc377c93692f2fe19fbad47eee6fb8f4cc98c56e935db5edb69806d84f6');

ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `users`
  ADD PRIMARY KEY (`ID`);

ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=340;

ALTER TABLE `users`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

````

### Step 2: Upload files to server
**This process will be simplified in the future.**
1. Download the content of this repository
2. Open the config PHP and replace `YOUR_DB_NAME`, `YOUR_DB_USER`, `YOUR_DB_PASSWORD` with your credentials of the database.
3. Upload all files to your server e.g. via FTP.
4. Open the site in the browser and check wheather you can see the login page - otherwise something went wrong.

### Step 3: Login with demo users
When importing the demo data there are two demo users to login with:<br>
User 1: `bennet`, Password: `test`<br>
User 2: `sophie`. Password: `test2`

### Step 4: Change login credentials
**This process will be simplified and more secure the future.**
To change the login names and passwords for the app how have unfortunately to login in your database and edit the entries in the table `users`.
1. Change the username by editing the entry and typing the new one
2. Change the password by editing the entry and pasting the double SHA256 hash of it (**will be optimised**)

**How to get the correct double sha256 hash?**
1. Open the app websites login page.
2. Open the developer tools
3. Navigate to "console"
4. Enter the following to get the hash for the database: <br>
`SHA256(SHA256('YOUR_PASSWORD_FOR_A_USER'));`

Alternatively you can use a online service like this [SHA256 generator](https://emn178.github.io/online-tools/sha256.html) or other tools.
It's important that you hash the password twice: 1. hash_of_password, 2. hash_of_password_hash
(**Will be optimised and rewritten in the future**)

**Delete a user**
Login to your database and delete the entry in `users` for the user you want to delete.
